Name: fairmq
Version: 1.4.55
Release: 1%{?dist}
Summary: C++ Message Queuing Library and Framework

License: LGPLv3
%define github github.com
%define gh_user FairRootGroup
%define gh_repo FairMQ
%define gh_repo_url https://%{github}/%{gh_user}/%{gh_repo}
URL: %{gh_repo_url}
Source0: %{name}-%{version}.tar.gz

BuildRequires: asiofi-devel
BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: faircmakemodules
BuildRequires: fairlogger-devel
BuildRequires: gcc-c++
BuildRequires: git
BuildRequires: ninja-build
BuildRequires: zeromq-devel

%description
FairMQ is designed to help implementing large-scale data processing
workflows needed in next-generation Particle Physics experiments. FairMQ
is written in C++ and aims to

* provide an asynchronous message passing abstraction of different data transport technologies,
* provide a reasonably efficient data transport service (zero-copy, high throughput),
* be data format agnostic, and
* provide basic building blocks that can be used to implement higher level data processing workflows.

%prep
%autosetup

%build
%cmake -G Ninja \
       -DDISABLE_COLOR=ON \
       -DBUILD_OFI_TRANSPORT=ON
%cmake_build

%install
%cmake_install

%files
%license LICENSE
%{_libdir}/libFairMQ*.so*
%{_bindir}/fairmq-bsampler
%{_bindir}/fairmq-merger
%{_bindir}/fairmq-multiplier
%{_bindir}/fairmq-proxy
%{_bindir}/fairmq-shmmonitor
%{_bindir}/fairmq-sink
%{_bindir}/fairmq-splitter
%{_bindir}/fairmq-uuid-gen


%package devel
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: boost-devel
Requires: fairlogger-devel
Summary: Development files for %{name}

%description devel
This package contains the header files and CMake package for developing against FairMQ.

%files devel
%license LICENSE
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%dir %{_includedir}/%{name}/options
%{_includedir}/%{name}/options/*.h
%dir %{_includedir}/%{name}/shmem
%{_includedir}/%{name}/shmem/*.h
%dir %{_includedir}/%{name}/tools
%{_includedir}/%{name}/tools/*.h
%dir %{_libdir}/cmake
%dir %{_libdir}/cmake/%{gh_repo}-%{version}
%{_libdir}/cmake/%{gh_repo}-%{version}/%{gh_repo}*.cmake
%{_datadir}/%{name}/cmake


%package examples
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: bash
Requires: xterm
Summary: Example files for %{name}

%description examples
This package contains example topologies for FairMQ.

%files examples
%license LICENSE
%{_bindir}/fairmq-ex-*
%{_bindir}/fairmq-start-ex-*.sh
%{_datadir}/%{name}/ex-*

%changelog
* Fri Sep 23 2022 Dennis Klein <d.klein@gsi.de> - 1.4.55-1
- Package v1.4.55
* Wed Jun  8 2022 Dennis Klein <d.klein@gsi.de> - 1.4.51-1
- Build debuginfo package
- Build with ninja and use cmake RPM macros
- Remove obsolete dependency to flatbuffers
* Mon Mar 21 2022 Dennis Klein <d.klein@gsi.de> - 1.4.50-1
- Package v1.4.50
* Thu Sep 9 2021 Dennis Klein <d.klein@gsi.de> - 1.4.41-1
- Package v1.4.41
* Tue May 11 2021 Dennis Klein <d.klein@gsi.de> - 1.4.37-1
- Package v1.4.37
